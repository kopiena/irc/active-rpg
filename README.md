# About
This bot rewards XP for talking in shared IRC channels. Levels are rewarded for earning XP.

Active-RPG is incomplete and pull requests are welcome.

# Installation
* Clone the repository and `cd` into it's directory.
* Edit config.json.example and rename it to config.json
* Run `chmod +x bot.js` to make the bot executable.
* To run your bot, type `./bot.js`.

# Commands
* `!score [<nickname>]` - Gives the score for the current user if no nickname is supplied.
## Admin Commands
* `!score.set <nickname> <xp>` - Gives the given nickname the given amount of XP.
* `!eval <string>` - Evaluates nodejs in the given string and prints output to console.
* `!flush` - Flushes scores to disk.