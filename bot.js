#!/usr/bin/node
var fs = require('fs');
var config = require("./config.json");
try
{
    var scoreboard = require("./scores.json");
} catch(e) {
    console.log("Scores do not exist or are invalid. Creating new scores file.")
    var scoreboard = JSON.parse('{}');
}
var lastmsg = [];
var irc = require('irc');
var client = new irc.Client(config['irc/server'], config['irc/nickname'], {
    port: config['irc/port'],
    secure: config['irc/secure'],
    channels: config['irc/channels'],
});


setInterval(function(){
    flushScores();
  }, config['flush_timer'] * 1000);  

setInterval(function(){
    Object.keys(scoreboard).forEach(function (player) {
        if(determineLvl(scoreboard[player]) >= config['voice_lvl'])
        {
            Object.keys(client.chans).forEach(function (channel) {

                Object.keys(client.chans[channel]['users']).forEach(function(nickname){
                    if(client.chans[channel]['users'][player] == '')
                    {
                        console.log("Voicing " + player + " in " + channel);
                        client.send("MODE", channel, "+v", player);
                    }
                });
            });
        }
    });
}, 60000);

client.addListener('error', function(message){
    console.log(message);
});

client.addListener('nick', function(oldnick, newnick, channels, message) {
    if(typeof scoreboard[newnick] != 'undefined')
    {
        scoreboard[newnick] = scoreboard[oldnick];
        scoreboard[oldnick] = undefined;
        console.log("Nick change from " + oldnick + " to " + newnick);
    } else {
        console.log("Nick change from " + oldnick + " to " + newnick);
        console.log("Record for " + newnick + " already exists.");
        console.log("Deleting record for " + oldnick + " score was " + scoreboard[oldnick]);
        scoreboard[newnick] = scoreboard[newnick]+scoreboard[oldnick];
        scoreboard[oldnick] = undefined;
    }
});

client.addListener('join', function(channel,nick,message) {
    if(determineLvl(scoreboard[nick.toLowerCase()]) >= config['voice_lvl'])
    {
        client.send("MODE", channel, "+v", nick);
    }
});

client.addListener('action', function (from, to, message)
{
    //score user
    if(typeof scoreboard[from.toLowerCase()] == 'undefined')
    {
        scoreboard[from.toLowerCase()] = Math.floor(Math.random() * 100) + 1;
        return;
    }
    var d = Number(new Date());
    if(typeof lastmsg[from.toLowerCase()] == 'undefined')
    {
        lastmsg[from.toLowerCase()] = d-31000;
    }
    datediff =  d - lastmsg[from.toLowerCase()];
    if( datediff >= 30000 )
    {
        points = Math.floor(Math.random() * (determineLvl(scoreboard[from.toLowerCase()]) + 100) + determineLvl(scoreboard[from.toLowerCase()]));
        scoreboard[from.toLowerCase()] = scoreboard[from.toLowerCase()] +  points;
        lastmsg[from.toLowerCase()] = d;
        return;
    }
    return;
});

client.addListener('message', function (from, to, message) {
    if(typeof from == 'undefined') { return; }
    console.log(from + ' => ' + to + ': ' + message);
    msg =  message.split(" ");
    switch(msg[0])
    {
        case "!score":
            if(msg[1])
            {
                if(typeof scoreboard[msg[1].toLowerCase()] == 'undefined')
                {
                    client.say(to, from + ", I do not recognize that nickname.");
                } else {
                    client.say(to, "Current XP for " + msg[1] + " is " + scoreboard[msg[1].toLowerCase()] + ". " + msg[1] + " is level " + determineLvl(scoreboard[msg[1].toLowerCase()]));
                }
            } else {
                if(typeof scoreboard[from.toLowerCase()] == 'undefined')
                {
                    client.say(to, from + ", you are not scored yet, please try talking.");
                } else {
                    client.say(to, "You currently have " + scoreboard[from.toLowerCase()] + " XP and you are level " + determineLvl(scoreboard[from.toLowerCase()]));
                }
            }
            break;
        case "!flush":
            config['admins'].forEach(function (item,index) {
                if(item.toLowerCase() == from.toLowerCase())
                {
                    flushScores();
                    client.say(to, from + ": I have flushed the current scores to disk.");
                }
            });
            break;
        case "!eval":
            if(config['allow_eval'] == true)
            {
                config['admins'].forEach(function (item,index) {
                    if(item.toLowerCase() == from.toLowerCase())
                    {
                        msg.shift();
                        try
                        {
                            console.log(eval(msg.join(" ")));
                        } catch(e)
                        {
                            console.log(e.message);
                        }
                    }
                });
            }
            break;
        case "!score.set":
            if(!msg[2]) { break; }
            config['admins'].forEach(function (item,index) {
                if(item.toLowerCase() == from.toLowerCase())
                {
                    scoreboard[msg[1].toLowerCase()] = msg[2];
                    client.say(to, from + ": XP for " + msg[1] + " is now set to " + scoreboard[msg[1].toLowerCase()] + ".");
                }
            });
            break;
        default:
            //score user
            if(typeof scoreboard[from.toLowerCase()] == 'undefined')
            {
                scoreboard[from.toLowerCase()] = Math.floor(Math.random() * 100) + 1;
                break;
            }
            var d = Number(new Date());
            if(typeof lastmsg[from.toLowerCase()] == 'undefined')
            {
                lastmsg[from.toLowerCase()] = d-31000;
            }
            datediff =  d - lastmsg[from.toLowerCase()];
            if( datediff >= 30000 )
            {
                points = Math.floor(Math.random() * (determineLvl(scoreboard[from.toLowerCase()]) + 100) + determineLvl(scoreboard[from.toLowerCase()]));
                scoreboard[from.toLowerCase()] = scoreboard[from.toLowerCase()] +  points;
                lastmsg[from.toLowerCase()] = d;
                break;
            }
            break;
    }
});

function determineLvl(xp)
{
    var lvl = 0;
    var i=0;
    if(xp < 4)
    {
        return 0;
    }
    while(i <= xp)
    {
        if(Math.sqrt(i) % 4 == 0)
        {
            lvl++;
        }
        i++;
    }
    return lvl;
}

function flushScores()
{
    fs.writeFile("./scores.json", JSON.stringify(scoreboard), function(err) {
        if(err) {
            return console.log(err);
        }
    
        console.log("Flushed scores to database.");
    }); 
}